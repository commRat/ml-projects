import numpy as np
import matplotlib.pyplot as plt

# THIS PROJECT IS ABOUT TO MAKE A 4 BOTS (NAMED BY OLD TV CARTOON CHARACTERS), 
# GIVES THEM A RANDOM CHOICE BETWEEN (-10 & 30). STORE THEIR CHOICES (50) AS 
# LISTS AND MAKE A GRAPH WITH THEIR RESULTS. THIS SCRIPT IS USING BOTS WITH  
# RNG. NEXT SCRIPT WILL BE USING TRAINED BOTS (DEEP LEARNING), SO WE WILL 
# BE ABLE TO SEE DIFFERENCES. GOAL IS TO ACHIEVE THE HIGHEST NUMBER POSSIBLE,
# BUT WITHOUT REPETITION OF THE PREVIOUS PICK. 

# Predict takes a list of each bot, his minimum number prediction and maximum
# number prediction. 
def predict(pred_list, num_1, num_2):
    last_elements = [] # Stored last elements
    global i
    x = 0
    i = 0

    while x < 50:
        prediction = np.random.randint(num_1, num_2) # Rng number
        if x > 0 and last_elements[-1] == prediction:
            prediction = 0 # We set prediction to 0 if it repeats 
        i = prediction + i
        pred_list.append(i)  # Prediction will append in bot's list
        print(prediction)
        last_elements.append(prediction)
        x += 1

# Creates a list of each bot
jerry = []
pluto = []
smurfette = []
daffy = []

# Creates specific color of each bot (for plt)
jerry_col = 'chocolate'
pluto_col = 'gold'
smurfette_col = 'cornflowerblue'
daffy_col = 'black'

# Creates (min & max) rng pick for each robot
jerry_min = -10
jerry_max = 30
pluto_min = -10
pluto_max = 30
smurfette_min = -10
smurfette_max = 30
daffy_min = -10
daffy_max = 30

# Calls the method for each bot
predict(jerry, jerry_min, jerry_max)
predict(pluto, pluto_min, pluto_max)
predict(smurfette, smurfette_min, smurfette_max)
predict(daffy, daffy_min, daffy_max)

# Creates an plot image
plt.plot(jerry, jerry_col, label='JERRY')
plt.plot(pluto, pluto_col, label='PLUTO')
plt.plot(smurfette, smurfette_col, label='SMURFETTE')
plt.plot(daffy, daffy_col, label='DAFFY')
plt.axis([1, 50, -20, 700])
plt.legend()
plt.show()

# Print the lists of current results
print(jerry)
print(pluto)
print(smurfette)
print(daffy)
