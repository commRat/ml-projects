from keras.datasets import imdb
import numpy as np
import string
from keras import models
from keras.models import Sequential
from keras import layers
from keras import optimizers
from keras import losses
from keras import metrics
import matplotlib.pyplot as plt
from keras.layers import Embedding, Flatten, Dense
from keras import preprocessing
from keras.optimizers import RMSprop
import os
import tensorflow as tf


# Nesting in format (number of tokens, max word index)
embedding_layer = Embedding(1000, 64)
    
max_features = 1000 # Number of words, which we use as symptoms
maxlen = 500 # Number of words, after which the text is cropped

# Load a data.
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
    
# Reverse order.
x_train = [x[::-1] for x in x_train]
x_test = [x[::-1] for x in x_test]
    
# Transform list to 2D numpy array.
x_train = preprocessing.sequence.pad_sequences(x_train, maxlen=maxlen)
x_test = preprocessing.sequence.pad_sequences(x_test, maxlen=maxlen)
    
model = Sequential()
model.add(Embedding(max_features, 128)) 
model.add(layers.LSTM(32))
model.add(layers.Dense(1, activation='sigmoid')) # Sigmoid output is represented from 0 to 1
    
model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['acc'])
    
#model.summary()
    
history = model.fit(x_train, y_train, epochs=6, batch_size=128, validation_split=0.2)

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']
    
epochs = range(1, len(acc) + 1)
    
plt.plot(epochs, loss, 'bo', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.legend()
plt.show()
    

